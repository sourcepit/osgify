/**
 * Copyright (c) 2011 Sourcepit.org contributors and others. All rights reserved. This program and the accompanying
 * materials are made available under the terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 */

package org.sourcepit.osgify.java;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Java Project</b></em>'.
 * <!-- end-user-doc -->
 * 
 * 
 * @see org.sourcepit.osgify.java.JavaModelPackage#getJavaProject()
 * @model
 * @generated
 */
public interface JavaProject extends JavaPackageBundle
{
} // JavaProject
